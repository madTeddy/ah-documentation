.. default-domain:: sphinxsharp

GameManager
============

.. namespace:: AHG.QuizRedux

.. type:: public class GameManager : MonoBehaviour

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using System.Collections;
    using UnityEngine;
    using UnityEngine.Events;

Описание
--------

Этот класс является **игровым менеджером**, который:

* Инициализирует всю игру на старте.
* Имеет ссылки на другие **важные объекты**.

.. attention:: Этот объект является `одиночкой <https://ru.wikipedia.org/wiki/Одиночка_(шаблон_проектирования)>`_.

Приватные сериализуемые поля
----------------------------

.. note:: Все сериализуемые значения изменяются в **инспекторе**.

.. variable:: private Config config

    Ссылка на экземпляр конфигурационного объекта.

.. variable:: private Animator headerAnimator

    Animator, который находится на Header объекте, и контроллирует все анимации.

---------------

Свойства и поля
---------------

.. property:: public static GameManager Instance { get; private set; }

    Свойство для доступа к **одиночному экземпляру** менеджера из любого класса.

.. property:: public Animator HeaderAnimator { get; }

    Свойство для доступа к объекту :var:`главного аниматора <headerAnimator>`.

------------------

Статические методы
------------------

.. method:: public static Config GetConfig()

    Возвращает ссылку на текущий конфигурационный объект.

.. note:: Эти методы являются **статическими**, для более краткого способа обращения к ним. Объекты же возвращаются из единственного экземпляра менеджера.

----------------

Публичные методы
----------------

.. method:: public void Play()

    Метод при нажатии на кнопку Play.

.. method:: public void PlayAnimation(string name, int layer = 0)
    :param(1): Название анимации.
    :param(2): Идентификатор слоя в аниматоре.

    Метод для запуска анимации из :var:`главного аниматора <headerAnimator>`.

.. method:: public void ToggleHeader(bool drop)
    :param(1): Опустить шапку игры?

    Опустить либо поднять шапку игры.

    * ``true`` - Опустить шапку игры.
    * ``false`` - Поднять шапку игры.

.. method:: public float GetAnimationLength(string name)
    :param(1): Название анимации.

    Возвращает длительность анимации в :var:`главном аниматоре <headerAnimator>`.

.. method:: public void WaitForSeconds(float seconds, UnityAction onEnd)
    :param(1): Длительность ожидания в секундах.
    :param(2): Действие при окончании ожидания.

    Выполнить действие через указанное количество секунд.