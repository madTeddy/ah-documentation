.. default-domain:: sphinxsharp

Colors
======

.. namespace:: AHG.QuizRedux

.. type:: public struct Colors

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using UnityEngine;

Описание
--------

Структура цветов, а так же полезных методов для работы с ними.

Свойства и поля
---------------
        
.. variable:: public static readonly Color Green = From255(46, 204, 113)

.. variable:: public static readonly Color LightRed = From255(231, 76, 60)

------

Методы
------

.. method:: public static Color From255(float r, float g, float b, float a)
    :param(1): Красный канал в диапозоне от 0 до 255.
    :param(2): Зелёный канал в диапозоне от 0 до 255.
    :param(3): Синий канал в диапозоне от 0 до 255.
    :param(4): Альфа канал в диапозоне от 0 до 255.

    Возвращает цвет в диапозоне от 0 до 1.

.. method:: public static Color From255(float r, float g, float b)
    :param(1): Красный канал в диапозоне от 0 до 255
    :param(2): Зелёный канал в диапозоне от 0 до 255
    :param(3): Синий канал в диапозоне от 0 до 255

    Возвращает цвет в диапозоне от 0 до 1

.. method:: public static string ToHex(Color color)
    :param(1): Репрезентация RGBA цветов.

    Возвращает цвет в **шестнадцатеричном** формате.