Перечисления
============

.. default-domain:: sphinxsharp

AnswerType
----------

.. namespace:: AHG.QuizRedux

.. enum:: public enum AnswerType
    :values: Correct Wrong TimeOver
    :val(1): Правильный ответ.
    :val(2): Не правильный ответ.
    :val(3): Закончилось время.

    Тип ответа на вопрос.