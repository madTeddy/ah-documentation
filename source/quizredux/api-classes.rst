Классы
======

.. toctree::
    :maxdepth: 1

    classes/categories-manager
    classes/category
    classes/config
    classes/gamemanager
    classes/question
    classes/quiz-manager