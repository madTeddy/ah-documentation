.. default-domain:: sphinxsharp

CaseViewer
==========

.. namespace:: AHG.CaseSimulatorRedux

.. type:: public class CaseViewer : MonoBehaviour

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using UnityEngine;
    using UnityEngine.UI;

Описание
--------

Этот класс должен находиться на **префабе** кейса, для его отображения и дальнейшей обработки кликов.

Приватные сериализуемые поля
----------------------------

.. note:: Все сериализуемые значения изменяются в **инспекторе**.

.. variable:: private Text titleText

    Ссылка на **UI Text**, для текста с названием кейса.

.. variable:: private Image icon

    Ссылка на **UI Image**, для изображения кейса.

.. variable:: private Button buyBtn

    Ссылка на **UI Button**, который служит кнопкой для покупки кейса.

.. variable:: private Text buyBtnText

    Ссылка на **UI Text**, для текста на кнопке покупки кейса.

---------------

Свойства и поля
---------------

.. property:: public int ID { get; set; }

    Идентификатор кейса.

.. property:: public Text TitleText { get; }

    Ссылка на **UI Text**, для текста с названием кейса.

.. property:: public Button BuyBtn { get; }

    Ссылка на **UI Button**, который служит кнопкой для покупки кейса.

.. property:: public Text BuyBtnText { get; }

    Ссылка на **UI Text**, для текста на кнопке покупки кейса.

.. property:: public Image Icon { get; }

    Ссылка на **UI Image**, для изображения кейса.

----------------

Публичные методы
----------------

.. method:: public Case GetCase()

    Возвращает экземпляр кейса.

.. method:: public void Initialize(Case @case)
    :param(1): Экземпляр кейса для обработки.

    Инициализировать **CaseViewer** указанным экземпляром кейса.

.. method:: public void OnClick()

    Метод при нажатии на кнопку выбора кейса.