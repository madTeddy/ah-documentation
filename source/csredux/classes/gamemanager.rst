.. default-domain:: sphinxsharp

GameManager
============

.. namespace:: AHG.CaseSimulatorRedux

.. type:: public class GameManager : MonoBehaviour

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using System.Collections.Generic;
    using System;
    using UnityEngine;
    using UnityEngine.UI;

Описание
--------

Этот класс является **игровым менеджером**, который:

* Инициализирует всю игру на старте.
* Имеет ссылки на другие **важные объекты**.
* Вызывает :meth:`метод сохранения <Data.Save>` при выходе.

.. attention:: Этот объект является `одиночкой <https://ru.wikipedia.org/wiki/Одиночка_(шаблон_проектирования)>`_.

Приватные сериализуемые поля
----------------------------

.. note:: Все сериализуемые значения изменяются в **инспекторе**.

.. variable:: private Config config

    Ссылка на экземпляр конфигурационного объекта.

.. variable:: private CaseSelector caseSelector

    Ссылка на экземпляр селектора кейсов.

.. variable:: private CaseOpener caseOpener

    Ссылка на экземпляр объекта для открытия кейсов.

.. variable:: private GameObject menuPanel

    Ссылка на панель меню игры.

.. variable:: private Text moneyText

    Ссылка на **UI Text** для отображения игровой валюты.

---------------

Свойства и поля
---------------

.. property:: public static GameManager Instance { get; private set; }

    Свойство для доступа к **одиночному экземпляру** менеджера из любого класса.

.. property:: public GameObject MenuPanel { get; }

    Свойство для доступа к объекту :var:`панели меню <menuPanel>`.

------------------

Статические методы
------------------

.. method:: public static CaseSelector GetCaseSelector()

    Возвращает ссылку на экземпляр селектора кейсов.

.. method:: public static CaseOpener GetCaseOpener()

    Возвращает ссылку на экземпляр объекта открытия кейсов.

.. method:: public static Config GetConfig()

    Возвращает ссылку на текущий конфигурационный объект.

.. note:: Эти методы являются **статическими**, для более краткого способа обращения к ним. Объекты же возвращаются из единственного экземпляра менеджера.

----------------

Публичные методы
----------------

.. method:: public Weapon GetWeapon(int caseId, int weaponId)
    :param(1): Порядковый идентификатор кейса.
    :param(2): Порядковый идентификатор оружия.

    Возвращает оружие из кейса по указанным идентификаторам, если оно существует.

    .. warning:: При указании несуществующих идентификаторов, вы получите **ошибку**.

.. method:: public List<Weapon>[] SortWeaponsByRarity(List<Weapon> weapons)
    :param(1): Список оружий для сортировки.

    Возвращает список оружий, отсортированных по :enum:`типу редкости <Rarity>`.
