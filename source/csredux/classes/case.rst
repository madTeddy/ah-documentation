.. default-domain:: sphinxsharp

Case
====

.. namespace:: AHG.CaseSimulatorRedux

.. type:: public class Case : ScriptableObject

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using System.Collections.Generic;
    using UnityEngine;

Описание
--------

Этот **класс** хранит информацию **о кейсе**.

.. figure:: /_static/csredux/image4.png

    Вид объекта в инспекторе.

Приватные сериализуемые поля
----------------------------

.. note:: Все сериализуемые значения изменяются в **инспекторе**.

.. variable:: string name

    Название кейса.

.. variable:: double cost

    Стоимость кейса в **игровой валюте**.

    .. hint:: Можете не указывать её, если кейсы не покупаются за деньги.

.. variable:: Sprite sprite

    Изображение кейса.

.. variable:: List<Weapon> weapons

    Список оружий.

.. variable:: List<float> generateChances

    Шансы генерации оружий по :enum:`редкостям <Rarity>`. Указываются в процентах, сумма которых равна 100.

    .. note:: При создании нового кейса, этот список заполняется стандартными шансами выпадения из :var:`шаблона <Data.DefaultGenerateChances>`.

---------------

Свойства и поля
---------------

.. property:: public int ID { get; set; }

    Идентификатор кейса.

.. property:: public string Name { get; }

    Название кейса.

.. property:: public double Cost { get; }

    Стоимость кейса в **игровой валюте**.

.. property:: public List<Weapon> Weapons { get; }

    Список оружий.

.. property:: public List<float> GenerateChances { get; }

    Шансы генерации оружий по :enum:`редкостям <Rarity>`.

------

Публичные методы
----------------

.. method:: public Sprite GetSprite()

    Возвращает изображение кейса.