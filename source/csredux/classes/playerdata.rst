.. default-domain:: sphinxsharp

PlayerData
==========

.. namespace:: AHG.CaseSimulatorRedux

.. type:: public class PlayerData

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using System.Collections.Generic;

Описание
--------

Этот класс хранит в себе данные игрока.

Свойства и поля
---------------

.. property:: public double Money {get; set;}

    Количество игровой валюты у игрока.

.. property:: public Dictionary<int, Weapon> Weapons {get; private set;}

    Все оружия в инвентаре игрока.

    **Ключ, значение:**

    * Порядковый идентификатор в инвентаре.
    * Экземпляр оружия.

.. variable:: public event OnMoneyChanged OnMoneyChangedEvent

    Событие при изменении игровой валюты.

.. variable:: public event OnInventoryChanged OnInventoryChangedEvent

    Событие при изменении инвентаря.

------

Публичные методы
----------------

.. method:: public void AddWeapons(params Weapon[] weapons)
    :param(1): Оружия которые надо добавить.

    Метод для добавления оружия в инвентарь игрока.

.. method:: public void RemoveWeapons(params int[] ids)
    :param(1): Порядковые идентификаторы оружий в инвентаре.

    Метод для удаления оружий в инвентаре, если они существуют.

--------

Делегаты
--------

.. method:: public delegate void OnMoneyChanged(double value)
    :param(1): Текущее количество игровой валюты.

    Делегат изменений значения игровой валюты.

.. method:: public delegate void OnInventoryChanged(InventoryChangeState state)
    :param(1): Текущее состояние инвентаря.

    Делегат изменений инвентаря у игрока.

.. attention:: Делегаты служат основой для :var:`событий <OnMoneyChangedEvent>`.

------------

Конструкторы
------------

.. method:: public PlayerData(double money, Dictionary<int, Weapon> weapons)
    :param(1): Количество валюты, которое будет присвоено игроку.
    :param(2): Словарь оружий, который будет присвоен игроку.