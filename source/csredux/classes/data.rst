.. default-domain:: sphinxsharp

Data
====

.. namespace:: AHG.CaseSimulatorRedux

.. type:: public static class Data

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;

Описание
--------

**Статический** класс для управления данными игры.

**Через него вы можете:**

* Управлять данными игрока.
* Загружать данные игры.
* Сохранять данные игры.

Свойства и поля
---------------

.. property:: public PlayerData Player { get; private set; }

    Свойство с текущим экземпляром игрока.

.. variable:: public static readonly float[] DefaultGenerateChances = { 65.0f, 19.0f, 13.0f, 2.99f, 0.01f }

    Стандартный шаблон шансов генерации редкостей, который используется при создании нового кейса.

------------------

Статические методы
------------------

.. method:: public static void Load()

    Метод для загрузки всех данных игрока с файла сохранения, если он существует.

.. method:: public static void Save()

    Метод для сохранения данных игрока.

.. method:: public static bool DeleteSave()
    :returns: Статус удаления файла сохранения.

    Метод для удаления текущего сохранения, если оно существует.