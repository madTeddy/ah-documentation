.. default-domain:: sphinxsharp

AudioManager
============

.. namespace:: AHG.CaseSimulatorRedux

.. type:: public class AudioManager : MonoBehaviour

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using System.Collections.Generic;
    using UnityEngine;

Описание
--------

Этот класс служит для **управления звуковыми эффектами**.

.. attention:: Этот объект является `одиночкой <https://ru.wikipedia.org/wiki/Одиночка_(шаблон_проектирования)>`_.

Приватные сериализуемые поля
----------------------------

.. note:: Все сериализуемые значения изменяются в **инспекторе**.

.. variable:: private List<AudioClip> audioClips

    Список звуковых эффектов, которые будут использоваться в игре.

---------------

Свойства и поля
---------------

.. property:: public static AudioManager Instance { get; private set; }

    Свойство для доступа к **одиночному экземпляру** менеджера из любого класса.

.. property:: public static float Volume { get; set; }

    Свойство для управления громкостью текущего **Audio Source** в диапозоне от **0** до **1**.

.. property:: public static bool Mute { get; set; }

    Свойство для переключения состояния звука текущего **Audio Source**.

    * ``true`` - звук текущего **Audio Source** отключён.
    * ``false`` - звук текущего **Audio Source** включён.

------------------

Статические методы
------------------

.. method:: public static void PlayOneShot(string name)
    :param(1): Имя аудиоклипа для воспроизведения.

    Воспроизвести звук, если он есть в :var:`списке <audioClips>`.

.. note:: Эти методы являются **статическими**, для более краткого способа обращения к ним. Объекты же возвращаются из единственного экземпляра менеджера.

------

Публичные методы
----------------

.. method:: public void Play(string name)
    :param(1): Имя аудиоклипа для воспроизведения.

    Воспроизвести звук, если он есть в :var:`списке <audioClips>`.