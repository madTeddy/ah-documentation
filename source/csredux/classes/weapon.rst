.. default-domain:: sphinxsharp

Weapon
======

.. namespace:: AHG.CaseSimulatorRedux

.. type:: public class Weapon

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using UnityEngine;

Описание
--------

Этот **класс** хранит информацию **об оружии**.

Приватные сериализуемые поля
----------------------------

.. note:: Все сериализуемые значения изменяются в **инспекторе**.

.. variable:: string name

    Название оружия.

.. variable:: Rarity rarity

    Тип редкости оружия.

.. variable:: double cost

    Стоимость оружия в **игровой валюте**.

.. variable:: Sprite sprite

    Изображение оружия.

---------------

Свойства и поля
---------------

.. property:: public int ID { get; private set; }

    Идентификатор оружия, который задаётся при :meth:`инициализации кейсов <Config.InitializeCases>`.

.. property:: public int CaseID { get; private set; }

    Идентификатор родительского кейса, который задаётся при :meth:`инициализации кейсов <Config.InitializeCases>`.

.. property:: public string Name { get; }

    Название оружия.

.. property:: public double Cost { get; }

    Стоимость оружия в **игровой валюте**.

------

Публичные методы
----------------

.. method:: public Rarity GetRarity()

    Возвращает тип редкости оружия.

.. method:: public Sprite GetSprite()

    Возвращает изображение оружия.

.. method:: public void SetID(int caseID, int id)
    :param(1): Идентификатор родительского кейса.
    :param(2): Идентификатор оружия.

    Устанавливает идентификаторы для родительского кейса и самого оружия.