.. default-domain:: sphinxsharp

PageSelectorBase
================

.. namespace:: AHG.CaseSimulatorRedux

.. type:: public abstract class PageSelectorBase : MonoBehaviour

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

Описание
--------

**Базовый класс** для селекторов с переключением страниц.

.. attention:: Этот класс используется только для **наследования** от него, и дальнейшей реализации.

Приватные сериализуемые поля
----------------------------

.. note:: Все сериализуемые значения изменяются в **инспекторе**.

Настройки селектора
"""""""""""""""""""

.. variable:: protected int pageCapacity

    Количество слотов на странице.

Объекты
"""""""

.. variable:: protected Transform slotsParent

    Ссылка на **Transform** компонент объекта, который является родителем для :var:`слотов <slotPrefab>`.

.. variable:: protected GameObject slotPrefab

    Ссылка на префаб слота.

.. variable:: protected Button nextPageBtn

    Ссылка на **UI Button**, который служит кнопкой переключения страницы вперёд.

.. variable:: protected Button prevPageBtn

    Ссылка на **UI Button**, который служит кнопкой переключения страницы назад.

---------------

Свойства и поля
---------------

.. property:: protected abstract int SlotsCount { get; }

    Общее количество слотов, которое надо отобразить в селекторе.

    .. important:: Это свойство нужно **реализовать** дочернему объекту.

------

Публичные методы
----------------

.. note:: ``virtual`` - возможно переопределение метода в дочерних объектах.

.. method:: public virtual void Load()

    Инициализировать слоты и открыть первую страницу.

.. method:: public virtual void Clear()

    Очистить пул слотов.

.. method:: public virtual void ChangePage(int dir)
    :param(1): Направление смены страницы.

    Сменить страницу в зависимости от направления.

    * ``-1`` - прошлая страница.
    * ``1`` - следующая страница.

.. method:: public virtual void GoToPage(int page)
    :param(1): Индекс страницы.

    Сменить текущую страницу на указанную, если она существует.

.. method:: protected abstract void InitializeSlot(int index, int id, GameObject slot)
    :param(1): Порядковый индекс слота.
    :param(2): Идентификатор слота относительно общего кол-ва слотов.
    :param(3): Объект слота.

    Этот метод вызывается при генерации страницы, чтобы обработать слот.

    .. important:: Этот метод нужно **реализовать** дочернему объекту.

Пример обработки слота
""""""""""""""""""""""

.. code-block:: csharp

    protected override void InitializeSlot(int index, int id, GameObject slot)
    {
        // Берём кейс по общему идентификатору слота.
        Case @case = GameManager.GetConfig().GetCase(id);
        // У слота в компоненте CaseViewer вызываем инициализацию.
        slot.GetComponent<CaseViewer>().Initialize(@case);
    }

Метод взят из класса :type:`CaseSelector`.