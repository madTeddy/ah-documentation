Перечисления
============

.. default-domain:: sphinxsharp

Rarity
------

.. namespace:: AHG.CaseSimulatorRedux

.. enum:: public enum Rarity
    :values: Blue Purple Pink Red Yellow
    :val(1): Армейское качество.
    :val(2): Запрещённое качество.
    :val(3): Засекреченное качество.
    :val(4): Тайное качество.
    :val(5): Редкое качество.

    Тип редкости выпадающего оружия.

--------------------

InventoryChangeState
--------------------

.. enum:: public enum InventoryChangeState
    :values: Add Remove
    :val(1): Состояние при добавлении оружия в инвентарь.
    :val(2): Состояние при удалении оружия из инвентаря.

    Состояние при изменении инвентаря.

---------

AlertType
---------

.. enum:: public enum AlertType
    :values: Default DoubleOption
    :val(1): Показать стандартное всплывающее окно с одной кнопкой.
    :val(2): Показать всплывающее окно с двумя вариантами действий.

    Тип всплывающего окна уведомлений.